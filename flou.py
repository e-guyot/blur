import cv2 
  
# calcul du flou d'un pixel donnée 
def computePx(image, depthmap, filtermoy, x, y, depthMax,depthMin):
    #filtermoy => création de la taille du filtre afin de réaliser la taille de la matrice en conséquence
    offset = filtermoy//2 # création du décalage
    r = 0
    g = 0
    b = 0
    sum =  0
    rows,cols,_ = image.shape
    for i in range(x-offset, offset+x):
        for j in range (y-offset, y+offset):
            if i >= 0 and j >= 0 and i < cols and j < rows:
                if depthMax <= depthmap.item(j, i) or depthMin >= depthmap.item(j, i):
                    b += image.item(j,i, 0)//(filtermoy**2)
                    g += image.item(j,i, 1)//(filtermoy**2)
                    r += image.item(j,i,2)//(filtermoy**2)
                    sum += 1/(filtermoy**2)
    return [b//sum, g//sum, r//sum]

def computeBlur(image, depthmap, depthMax, depthMin=0):
    imgBlur = image.copy()
    rows,cols,_ = image.shape # elemt shape[] contient ligne et colonne permet de recup rows et cols easy
    for i in range(cols):
        for j in range (rows):
            if depthMax <= depthmap.item(j, i) or depthMin >= depthmap.item(j, i):
                vect = computePx(image, depthmap, 5, i, j, depthMax,depthMin)
                imgBlur.itemset((j,i, 0), vect[0])
                imgBlur.itemset((j,i,1), vect[1])
                imgBlur.itemset((j,i,2), vect[2])
    return imgBlur

image = cv2.imread('./img/view1.png') 
depthmap = cv2.imread('./img/disp1.png', 0) #0 => matrice en nuance de gris une valeur en nuance de gris
blur = computeBlur(image, depthmap, 150, 100)
# depth = compris entre 0 et 256  0 => 1er plan 
  
cv2.imshow('Original Image', image) 
cv2.imshow('Depth Map', depthmap)
cv2.imshow('Blur Image', blur)  
cv2.waitKey(0) 